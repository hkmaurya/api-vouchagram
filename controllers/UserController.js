/*******************************************************************************
 * User Controller
 ******************************************************************************/
 'use strict';
 
 const User = require('../models/User');
 const commonHelper = require('../helpers/common');
 const userTransformer = require('../transformers/user');
 const userHelper = require('../helpers/user');
 const Q = require('q');

 // Create and Save a new User
const signup = async (req, res) => {
  const { userType } = req.auth.credentials;
  if(userType != 1) return res.boom.conflict(res.__('adminTypeError'));
  let { email, firstName, lastName, password } = req.body;
  email = commonHelper.toLowerCase(commonHelper.removeEmptySpace(email))
  const isValid = commonHelper.isEmailValid(email)
  if (!isValid) return res.boom.conflict(res.__('InvalidEmail'));
  const userInfo = await User.emailExist(email);
  if (userInfo) return res.boom.conflict(res.__('EmailTaken'));
  const user = new User({
    firstName,
    lastName,
    email,
    userType: '2',
    fullName: `${firstName} ${lastName}`,
    password: userHelper.hashPassword(password)
  });
  // Save User in the database
  User.create(user, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    else res.send(data);
  });
};

 // Login user
 const userLogin = async (req, res) => {
  let { email, password } = req.body;
  email = commonHelper.toLowerCase(commonHelper.removeEmptySpace(email))
  const isValid = commonHelper.isEmailValid(email)
  if (!isValid) return res.boom.conflict(res.__('InvalidEmail'));
  const userInfo = await User.emailExist(email);
  if (!userInfo) return res.boom.conflict(res.__('InvalidLogin'));
  const isMatchPassword = userHelper.comparePassword(password, userInfo.password);
  if (!isMatchPassword) return res.boom.unauthorized(res.__('InvalidLogin'));
  res.status(200).send({
    success: true,
    token: userHelper.getAuthTokens(userInfo),
    message: res.__('LoginSuccess'),
    data: userTransformer.userloginDetails(userInfo),
  });
};

const getUsersList = async (req, res) => {
  try {
    const { userType } = req.auth.credentials;
    if(userType != 1) return res.boom.conflict(res.__('adminTypeError'));
    let offset = parseInt(req.query.page ? (req.query.page - 1) * req.query.limit : 0);
    let limit = parseInt(req.query.limit ? req.query.limit : 10);
    const users = await Q.all([ User.countData(req.query.name), User.getAll(req.query.name, offset, limit) ]);
    let results = userTransformer.userList(users[1]);
    const result = {
      success: true,
      message: res.__('UserListing'),
      data: results,
      totalCount: users[0],
    };
    return res.status(200).send(result);
  } catch (err) {
    return res.boom.badRequest(err);
  }
};

const getUserDetails = async (req, res) => {
  try {
    const { userType } = req.auth.credentials;
    if(userType != 1) return res.boom.conflict(res.__('adminTypeError'));
    const { id } = req.params;
    let user = await User.findOne(id);
    if (!user) return res.boom.notFound(res.__('InvalidId'));
    let results = userTransformer.userProfile(user);
    res.status(200).send({
      success: true,
      message: res.__('UserDetails'),
      data: results
    });
  } catch (err) {
    return res.boom.badRequest(err);
  }
};


const update = async (req, res) => {
  try {
    const { userType } = req.auth.credentials;
    if(userType != 1) return res.boom.conflict(res.__('adminTypeError'));
    const { id } = req.params;
    let { firstName, lastName } = req.body;
    let fullName = `${firstName} ${lastName}`;
    const userObj = { firstName, lastName, fullName };
    let user = await Q.all([User.findOne(id), User.update(id, userObj)]);
    console.log(user[0], 'user++++')
    if (!user[0]) return res.boom.notFound(res.__('InvalidId'));
    let results = userTransformer.userProfile(user[1]);
    res.status(200).send({
      success: true,
      message: res.__('UserDetails'),
      data: results
    });
  } catch (err) {
    return res.boom.badRequest(err);
  }
};

const remove = async (req, res) => {
  try {
    const { userType } = req.auth.credentials;
    if(userType != 1) return res.boom.conflict(res.__('adminTypeError'));
    const { id } = req.params;
    let user = await Q.all([User.findOne(id), User.remove(id)]);
    if (!user[0]) return res.boom.notFound(res.__('InvalidId'));
    res.status(200).send({
      success: true,
      message: res.__('UserRemoved'),
      data: null
    });
  } catch (err) {
    return res.boom.badRequest(err);
  }
};

 // Create default admin 
 const createDefaultSuperAdmin = async () => {
  const users = [
    { email: 'raj.k@gyftr.com', firstName: 'Raj', lastName: 'Kumar', fullName: 'Raj Kumar', userType: '1', password: 'qwerty'},
    { email: 'Gautam@gyftr.com', firstName: 'Gautam', lastName: 'Kumar', fullName: 'Gautam Kumar', userType: '1', password: 'qwerty'}
  ];
  for (let i=0; i<users.length; i++) {
    const userInfo = await User.emailExist(users[i].email);
    if (userInfo) {
      console.log('Default admin created successfully');
    } else {
      const user = new User({
        firstName: users[i].firstName,
        lastName: users[i].lastName,
        email: users[i].email,
        userType: '1',
        fullName: `${users[i].firstName} ${users[i].lastName}`,
        password: userHelper.hashPassword(users[i].password)
      });
      // Save User in the database
      User.create(user, (err, data) => {
        if (err) {
          console.log('Error in create default admin');
        } else {
          console.log('Default admin created successfully');
        }
      });
    }
  }
};

module.exports = {
  signup,
  userLogin,
  getUsersList,
  getUserDetails,
  update,
  remove,
  createDefaultSuperAdmin
};