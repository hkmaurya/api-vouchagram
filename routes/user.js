'use strict';
const UserController = require('../controllers/UserController');
const auth = require('../helpers/auth').userValidate;
const validator = require('express-joi-validator');
const UserSchema = require('../validations/user-schema');

module.exports = (app) => {
  /**
   * @swagger
   *  /api/employee:
   *    post:
   *      summary: Add employee
   *      tags: [Employee]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/UserSignup'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/UserSignup'
   */

  app.post('/api/employee', validator(UserSchema.signup), auth, (req, res) => {
    UserController.signup(req, res);
  });

  /**
   * @swagger
   *  /api/user/login:
   *    post:
   *      summary: User Login
   *      tags: [User]
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/UserLogin'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/UserLogin'
   */
  app.post('/api/user/login', validator(UserSchema.Userlogin), (req, res) => {
    UserController.userLogin(req, res);
  });

  /**
   * @swagger
   *  /api/employee/{id}:
   *    get:
   *      summary: Employee details
   *      tags: [Employee]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *        - in: path
   *          name: id
   *          schema:
   *              type: string
   *          required: true
   *      responses:
   *        "200":
   *          description: Success
   */
   app.get('/api/employee/:id', auth, (req, res) => {
      UserController.getUserDetails(req, res);
  });

  /**
   * @swagger
   *  /api/employee/{id}:
   *    put:
   *      summary: Update Employee details
   *      tags: [Employee]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *        - in: path
   *          name: id
   *          schema:
   *              type: string
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/UserUpdate'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/UserUpdate'
   */
   app.put('/api/employee/:id', validator(UserSchema.update), auth, (req, res) => {
    UserController.update(req, res);
  });

  /**
   * @swagger
   *  /api/employees:
   *    get:
   *      summary: Employees List By Admin
   *      tags: [Employee]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *        - in: query
   *          name: name
   *          description: Search by name
   *          schema:
   *              type: string
   *        - in: query
   *          name: page
   *          description: Page number for pagination
   *          schema:
   *              type: number
   *        - in: query
   *          name: limit
   *          description: Limit for pagination
   *          schema:
   *              type: number
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/ListByUser'
   */
   app.get('/api/employees', auth, (req, res) => {
    UserController.getUsersList(req, res);
  });

  /**
   * @swagger
   *  /api/employee/{id}:
   *    delete:
   *      summary: Employee details removed
   *      tags: [Employee]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *        - in: path
   *          name: id
   *          schema:
   *              type: string
   *          required: true
   *      responses:
   *        "200":
   *          description: Success
   */
   app.delete('/api/employee/:id', auth, (req, res) => {
    UserController.remove(req, res);
});

};
