'use strict';

const userProfile = (user) => {
  let { id, email, firstName, lastName } = user;
  return { id, email, firstName, lastName };
};

const userloginDetails = (user) => {
  let { id, email, firstName, lastName } = user;
  return { id, email, firstName, lastName };
};

const userList = (users) => {
  return users.map(user => userProfile(user))
}

module.exports = {
  userProfile,
  userloginDetails,
  userList
};
