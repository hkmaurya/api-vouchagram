/*******************************************************************************
 * User Helper
 ******************************************************************************/

const Config = require('../config/config'),
  commonHelper = require('./common'),
  Jwt = require('jsonwebtoken'),
  bcrypt = require('bcrypt');

const jwtTokenVerify = (data) => {
  return new Promise((resolve, reject) => {
    Jwt.verify(data, Config.key.privateKey, (err, decoded) => {
      if (err) {
        resolve(false);
      } else {
        resolve(decoded);
      }
    });
  });
};

const comparePassword = (password, hashedPassword) => {
  password = commonHelper.removeEmptySpace(password);
  hashedPassword = commonHelper.removeEmptySpace(hashedPassword);
  return bcrypt.compareSync(password, hashedPassword);
};

const hashPassword = (password) => {
  password = commonHelper.removeEmptySpace(password);
  return bcrypt.hashSync(password, Config.saltRounds);
};

const getAuthTokens = (user) => {
  const { _id, firstName, lastName, email, userType, userStatus } = user;
  const jwtdata = { _id, firstName, lastName, email, userType, userStatus };
  return Jwt.sign(jwtdata, Config.key.privateKey, {
    expiresIn: Config.key.tokenExpiry,
  });
};

module.exports = {
  getAuthTokens,
  hashPassword,
  comparePassword,
  jwtTokenVerify
};
