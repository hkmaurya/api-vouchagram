'use strict';

const mysql = require("mysql");
const Config = require('../config/config');

// Create a connection to the database
const connection = mysql.createConnection({
    host: Config.mySqlDB.HOST,
    user: Config.mySqlDB.USER,
    password: Config.mySqlDB.PASSWORD,
    database: Config.mySqlDB.DB
  });
  
  // open the MySQL connection
connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});
  
module.exports = connection;