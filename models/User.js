/*******************************************************************************
 * User Model
 ******************************************************************************/
 'use strict';
 const sql = require('../lib/database');
 
 
 //Define User constructor
const User = function(user) {
  this.firstName = user.firstName;
  this.lastName = user.lastName;
  this.fullName = user.fullName;
  this.email = user.email,
  this.userType = user.userType,
  this.password = user.password
};

User.create = (newUser, result) => {
  sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

User.findOne = (id) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT * FROM users WHERE id = ${id} AND userType= '2'`, (err, res) => {
      if(err) {
        reject('Invalid user id');
      }
      if (res.length) {
        resolve(res[0])
      } else {
        reject('Invalid user id');
      }
    });
  });
}

User.remove = (id) => {
  return new Promise((resolve, reject) => {
    sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
      if(err) {
        reject('Invalid user id');
      }
      if (res.affectedRows == 0) {
        reject('Invalid user id');
      }
      resolve(res);
    });
  });
};

User.getAll = (name, offset, limit) => {
  return new Promise((resolve, reject) => {
    let query = "SELECT * FROM users";
    if (name) {
      query += ` WHERE (fullName LIKE '%${name}%' OR email LIKE '%${name}%') AND userType= '2' LIMIT ${offset},${limit}`;
    } else {
      query += ` WHERE userType= '2' LIMIT ${offset},${limit}`;
    }
    sql.query(query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        reject(err);
        return;
      }
      console.log("users: ", res);
      resolve(res);
    });
  });
}

User.update = (id, user) => {
  return new Promise((resolve, reject) => {
    sql.query("UPDATE users SET firstName = ?, lastName = ?, fullName = ? WHERE id = ?", [user.firstName, user.lastName, user.fullName, id], (err, res) => {
      if(err) {
        reject('Invalid user id');
      }
      if (res.affectedRows == 0) {
        reject('Invalid user id');
      }
      resolve({ id: id, ...user });
    });
  });
};

User.countData = (name) => {
  return new Promise((resolve, reject) => {
    let query = "SELECT COUNT(*) FROM users";
    if (name) {
      query += ` WHERE (fullName LIKE '%${name}%' OR email LIKE '%${name}%') AND userType= '2'`;
    } else {
      query += ` WHERE userType= '2'`;
    }
    sql.query(query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        reject(err);
        return;
      }
      console.log("users count: ", res);
      resolve(res[0]["COUNT(*)"]);
    });
  });
}

User.emailExist = (email) => {
  return new Promise((resolve, reject) => {    
    sql.query('SELECT * FROM users WHERE email="'+email+'"', (err, res) => {
      if(err) {
        console.log("error: ", err);
        resolve(true);
      }
      if (res.length) {
        resolve(res[0]);
      } else {
        resolve(false);
      }
    });
  });
};

module.exports = User;